# Trabalho de qualidade segundo bimestre

![build](https://gitlab.com/eliasreis54/qualidade_trabalho2bim/badges/main/pipeline.svg)
![codecov](https://codecov.io/gl/eliasreis54/qualidade_trabalho2bim/branch/main/graph/badge.svg?token=EW46NKZ0WT)

## O objetivo

Colocar em prática todo aprendizado sobre testes que foi passado em aula.

# Techs

* node.js
* jest

## Como executar

Assumindo que você tenha o [node.js](https://nodejs.org/en/) instalado na sua máquina.

### Download do código
ssh:
```
git clone git@gitlab.com:eliasreis54/qualidade_trabalho2bim.git
```

https
```
git clone https://gitlab.com/eliasreis54/qualidade_trabalho2bim.git
```

### Instalação das dependências

No diretório raiz

```
npm i
```

### Execução dos testes

somente execução:

```
npm test
```

Execução e relatório de cobertura:

```
npm run test:cover
```
