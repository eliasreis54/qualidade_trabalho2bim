const canUserCanDriveByType = (user, vehicleType) => user.driveLicenceCategory.includes(vehicleType.driveLicence)

const canUserCanDriveByAge = (user, vehicleType, currentYear) => {
  const userAge = currentYear - user.bornYear
  return userAge >= vehicleType.age
}

const printUserList = userList => {
  userList.forEach(user => {
    console.log(`user: ${user.name}, hasUserDriveLicence: ${user.hasUserDriveLicence}`)
  })
}

const howManyYearToGetDriveLicence = (user, vehicleType, currentYear) => {
  const userAge = currentYear - user.bornYear
  const timeToWait = vehicleType.age - userAge
  if (timeToWait <= 0) {
    return 0
  }
  return timeToWait
}

const printUserCanDrive = (user, vehicleType, currentYear) => {
  if (user.hasUserDriveLicence) {
    if (canUserCanDriveByType(user, vehicleType)) {
      if (canUserCanDriveByAge(user, vehicleType, currentYear)) {
        console.log("Go a head to drive your car")
      } else {
        const yearsToDrive = howManyYearToGetDriveLicence(user, vehicleType, currentYear);
        console.log(`You can't drive it because you are young, wait ${yearsToDrive} years`)
      }
    } else {
      console.log("You can't drive it because you dont have a drive licence type")
    }
  } else {
    console.log('You dont have a drive licence')
  }
}

module.exports = {
  canUserCanDriveByType,
  canUserCanDriveByAge,
  printUserList,
  howManyYearToGetDriveLicence,
  printUserCanDrive,
}

