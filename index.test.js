const { canUserCanDriveByType, canUserCanDriveByAge, printUserList, howManyYearToGetDriveLicence, printUserCanDrive } = require('./index')

const { users, REQUIREMENTS_FOR_TYPE } = require('./data')

let consoleMock

beforeEach(() => {
  consoleMock = jest.spyOn(console, 'log').mockImplementation();
});


afterEach(() => {
  consoleMock.mockReset()
});

describe('(canUserCanDriveByType): Test if user can drive by type', () => {
  test('Should return true when user has correct driveLicence', () => {
    const canDriveUser0 = canUserCanDriveByType(users[0], REQUIREMENTS_FOR_TYPE.CAR)
    expect(canDriveUser0).toBe(true);

    const canDriveUser1 = canUserCanDriveByType(users[1], REQUIREMENTS_FOR_TYPE.CAR)
    expect(canDriveUser1).toBe(true);
  });

  test('Should return false when user has not correct driveLicence', () => {
    const canDriveUser0 = canUserCanDriveByType(users[0], REQUIREMENTS_FOR_TYPE.BUS)
    expect(canDriveUser0).toBe(false);

    const canDriveUser1 = canUserCanDriveByType(users[1], REQUIREMENTS_FOR_TYPE.BUS)
    expect(canDriveUser1).toBe(false);
  });
})

describe('(canUserCanDriveByAge): Test if user has age to drive specific type', () => {
  const currentYear = 2021
  test('Should return true when user has valid age', () => {
    const canDriveUser0 = canUserCanDriveByAge(users[0], REQUIREMENTS_FOR_TYPE.CAR, currentYear)
    expect(canDriveUser0).toBe(true);

    const canDriveUser1 = canUserCanDriveByAge(users[1], REQUIREMENTS_FOR_TYPE.CAR, currentYear)
    expect(canDriveUser1).toBe(true);
  });

  test('Should return false when user has invalid age', () => {
    const canDriveUser0 = canUserCanDriveByAge(users[0], REQUIREMENTS_FOR_TYPE.TRAILLER, currentYear)
    expect(canDriveUser0).toBe(false);

    const canDriveUser1 = canUserCanDriveByAge(users[1], REQUIREMENTS_FOR_TYPE.TRAILLER, currentYear)
    expect(canDriveUser1).toBe(false);
  });
})

describe('(printUserList): Test if printUserList call console.log with correct data', () => {
  test('Should print user in console', () => {

    printUserList(users)

    const calls = consoleMock.mock.calls

    expect(calls.length).toBe(2)
    expect(calls[0][0]).toBe(`user: ${users[0].name}, hasUserDriveLicence: ${users[0].hasUserDriveLicence}`)
    expect(calls[1][0]).toBe(`user: ${users[1].name}, hasUserDriveLicence: ${users[1].hasUserDriveLicence}`)
  });
})

describe('(howManyYearToGetDriveLicence): Test how many years the user need to wait to get a drive licence', () => {
  test(`Should return 1 when user is 27 and year is 2021`, () => {
    const yearToWait = howManyYearToGetDriveLicence(users[0], REQUIREMENTS_FOR_TYPE.TRAILLER, 2021)
    expect(yearToWait).toBe(1)
  });

  test(`Should return 2 when user is 26 and year is 2020`, () => {
    const yearToWait = howManyYearToGetDriveLicence(users[0], REQUIREMENTS_FOR_TYPE.TRAILLER, 2020)
    expect(yearToWait).toBe(2)
  });

  test(`Should return 0 when year is 2022`, () => {
    const yearToWait = howManyYearToGetDriveLicence(users[0], REQUIREMENTS_FOR_TYPE.TRAILLER, 2022)
    expect(yearToWait).toBe(0)
  });

  test(`Should return 0 when year is 2040`, () => {
    const yearToWait = howManyYearToGetDriveLicence(users[0], REQUIREMENTS_FOR_TYPE.TRAILLER, 2040)
    expect(yearToWait).toBe(0)
  });
})

describe('(printUserCanDrive): Test if printUserCanDrive call console.log with correct data', () => {
  test('Should print Go a head to drive your car when all is ok', () => {

    printUserCanDrive(users[0], REQUIREMENTS_FOR_TYPE.CAR, 2021)

    const calls = consoleMock.mock.calls

    expect(calls.length).toBe(1)
    expect(calls[0][0]).toBe(`Go a head to drive your car`)
  });

  test('Should print user is young when user is young', () => {

    printUserCanDrive(users[0], REQUIREMENTS_FOR_TYPE.CAR, 2000)

    const calls = consoleMock.mock.calls

    expect(calls.length).toBe(1)
    expect(calls[0][0]).toBe("You can't drive it because you are young, wait 12 years")
  });

  test('Should print user does not have a correct drive licence type', () => {

    printUserCanDrive(users[0], REQUIREMENTS_FOR_TYPE.TRAILLER, 2021)

    const calls = consoleMock.mock.calls

    expect(calls.length).toBe(1)
    expect(calls[0][0]).toBe("You can't drive it because you dont have a drive licence type")
  });

  test('Should print user does not have a correct drive licence', () => {

    printUserCanDrive(users[1], REQUIREMENTS_FOR_TYPE.TRAILLER, 2021)

    const calls = consoleMock.mock.calls

    expect(calls.length).toBe(1)
    expect(calls[0][0]).toBe("You dont have a drive licence")
  });
})
