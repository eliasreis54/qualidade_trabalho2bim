const DRIVE_LICENCE_TYPE = {
 A: 'A',
 B: 'B',
 C: 'C',
 D: 'D',
 E: 'E',
}

const REQUIREMENTS_FOR_TYPE = {
 CAR: {
  driveLicence: DRIVE_LICENCE_TYPE.B,
  age: 18
 },
 MOTORCYCLE: {
  driveLicence: DRIVE_LICENCE_TYPE.A,
  age: 18
 },
 TRUCK: {
  driveLicence: DRIVE_LICENCE_TYPE.C,
  age: 22
 },
 BUS: {
  driveLicence: DRIVE_LICENCE_TYPE.C,
  age: 25
 },
 TRAILLER: {
  driveLicence: DRIVE_LICENCE_TYPE.C,
  age: 28
 },
}

const elias = {
  name: 'Elias Reis',
  bornYear: 1994,
  hasUserDriveLicence: true,
  driveLicenceCategory: [DRIVE_LICENCE_TYPE.A, DRIVE_LICENCE_TYPE.B],
}

const pomps = {
  name: 'Felipe Pompeu',
  bornYear: 1994,
  hasUserDriveLicence: false,
  driveLicenceCategory: [DRIVE_LICENCE_TYPE.A, DRIVE_LICENCE_TYPE.B],
}

const users = [elias, pomps]

module.exports = {
  DRIVE_LICENCE_TYPE,
  REQUIREMENTS_FOR_TYPE, 
  users,
}
